from flask import Flask, render_template_string

app = Flask(__name__)

# Define the HTML template
html_template = """
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sample Webpage</title>
    <style>
        body { font-family: Arial, sans-serif; margin: 0; padding: 0; }
        header { background-color: #4CAF50; color: white; text-align: center; padding: 1em 0; }
        main { padding: 20px; }
        footer { background-color: #f1f1f1; text-align: center; padding: 10px 0; position: fixed; width: 100%; bottom: 0; }
    </style>
</head>
<body>
    <header>
        <h1>Welcome to the Sample Webpage</h1>
    </header>
    <main>
        <h2>Hello, World!</h2>
        <p>This is a simple web page created using Flask in Python.</p>
    </main>
    <footer>
        <p>&copy; 2024 Sample Webpage</p>
    </footer>
</body>
</html>
"""

@app.route('/')
def home():
    return render_template_string(html_template)

if __name__ == '__main__':
    app.run(debug=True)