import requests

# List of 10 popular cities
cities = ["New York", "Los Angeles", "Chicago", "Houston", "Phoenix", "Philadelphia", "San Antonio", "San Diego", "Dallas", "San Jose"]

# Your OpenWeatherMap API key
api_key = "your_api_key"

# Base URL for OpenWeatherMap API
base_url = "http://api.openweathermap.org/data/2.5/weather"

# Function to get weather data for a city
def get_weather(city, api_key):
    try:
        # Construct the full API URL
        url = f"{base_url}?q={city}&appid={api_key}&units=metric"
        response = requests.get(url)
        response.raise_for_status()  # Raise an HTTPError for bad responses
        data = response.json()
        return data
    except requests.exceptions.HTTPError as http_err:
        print(f"HTTP error occurred for {city}: {http_err}")
    except Exception as err:
        print(f"Other error occurred for {city}: {err}")

# Loop through each city and print its weather data
for city in cities:
    weather_data = get_weather(city, api_key)
    if weather_data:
        print(f"Weather in {city}:")
        print(f"  Temperature: {weather_data['main']['temp']}°C")
        print(f"  Weather: {weather_data['weather'][0]['description']}")
        print(f"  Humidity: {weather_data['main']['humidity']}%")
        print(f"  Wind Speed: {weather_data['wind']['speed']} m/s")
        print("="*40)